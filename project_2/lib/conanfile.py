from conans import ConanFile, CMake, tools

class TheAnswer(ConanFile):
    name = "the_answer"
    version = "0.1"
    license = "MIT"
    url = "example.com"
    description = "the answer to the ultimate question of life, the universe, and everything."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake_find_package"

    # remove if the C/C++ sources are not in the same repository as the conanfile.py
    # https://docs.conan.io/en/latest/creating_packages/package_repo.html
    exports_sources = "*"

    def source(self):
        # not needed, but here's an example: https://docs.conan.io/en/latest/creating_packages/getting_started.html
        # here we could eg patch stuff, depending on the compiler version.

        # tools.git.clone
        # tools.replace_in_file
        pass

    # https://docs.conan.io/en/latest/reference/conanfile/methods.html#requirements
    def requirements(self):
        self.requires("fmt/8.0.0")

    def build(self):
        cmake = CMake(self)
        # here we could add cxx flags, set cmake options, and more
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.hpp", dst="include")
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.dynlib", dst="lib", keep_path=False)

    def package_info(self):
        # stuff the generator needs
        self.cpp_info.libs = ["the_answer"]

