#include "fun.hpp"
#include <fmt/format.h>

std::string fun::the_answer() { return fmt::format("The answer is {}", 42); }
