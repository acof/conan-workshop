#!/bin/bash -e

cd "$(dirname "$0")"

conan create ../lib
conan install --generator cmake_find_package --install-folder dependencies .
rm -rf build
cmake -S . -B build -DCMAKE_MODULE_PATH=dependencies
cmake --build build/

