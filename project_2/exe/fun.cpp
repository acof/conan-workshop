#include <fun.hpp>
#include <iostream>

int main(int, char *[]) {
  std::cout << fun::the_answer() << std::endl;
  return 0;
}
