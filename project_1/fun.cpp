#include <iostream>
#include <nlohmann/json.hpp>

int main(int, char *[]) {
  auto fun = nlohmann::json::parse(R"({"happy": true, "answer": 42})");
  std::cout << "The answer is " << fun["answer"] << std::endl;
  return 0;
}
