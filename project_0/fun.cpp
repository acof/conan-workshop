#include <fmt/format.h>
#include <iostream>

int main(int, char *[]) {
  std::cout << fmt::format("The answer is {}.", 42) << std::endl;
  return 0;
}
