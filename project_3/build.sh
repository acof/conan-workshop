#!/bin/bash -e

cd "$(dirname "$0")"

rm -rf build dependencies
conan install --generator cmake_find_package --install-folder dependencies --build missing --remote conan-center --profile profile_release .
conan build --install-folder dependencies --build-folder build .

