from conans import ConanFile, CMake, tools

class TheAnswer(ConanFile):
    name = "fun"
    version = "0.1"

    license = "MIT"
    url = "example.com"
    description = "the answer to the ultimate question of life, the universe, and everything."

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}

    generators = "cmake_find_package"
    exports_sources = "*"

    def source(self):
        pass

    def requirements(self):
        self.requires("the_answer/0.1")

    def build(self):
        cmake = CMake(self)
        # here we could add cxx flags, set cmake options, and more
        cmake.configure()
        cmake.build()

    def package(self):
        pass

    def package_info(self):
        pass

