# conan workshop

## welcome, who is who?

- what's your experience with `C++11`, `CMake`?
- what's your experience with `C++` dependency managment so far?
- what's your experience with other dependency mangement sofware in other languages?
- how much did you prepare for today? 😇

## lingua

- host system - is the system the code runs on.
- build system - is the system the code compiles on.

## why conan?

- cross compilation
- lot's of well tested receipies (build scripts)
- c++ is old and complicated and so is their legacy.
- There are a lot of build-systems out there! eg. CMake, autotools, meson, Visual Studio, XCode, gyp, make... Conan has a lot of helper to handle these.
- aka conan manages the underlying build system
- decentral
- open source, free, well tested
- awesome community with very fast and kind support-response times.
- One does not simply change or update a dependency - conan to the resuce!
- Don't underestimate dependency managment. I met companies who have been stuck with gcc4.8 because they would not dare to recompile `boost`. But if depenedency mangament would be easy this would be less of a problem.
- Because of a missing dependency managment system we tend to re-implement stuff, than rather re-use stuff
- We don't need to intergrate conan into the build system to use it

## installation

install conan

```bash
apt install -y python3-pip
pip3 install --user conan
# ensure `~/.local/bin` is in `PATH`
export PATH="${HOME}/.local/bin:$PATH"
conan --version
```

set up the default profile. More on profiles later.

```bash
conan profile new default --detect --force
# due to a breaking ABI change in gcc5s libc++ (std::string) we have to set the following setting
conan profile update settings.compiler.libcxx=libstdc++11 default
```

add storz artifactory

```bash
# by using `--insert 0` we ensure that storz artifactory is the first one consulted.
conan remote add --insert 0 -f storz https://brm-01.karlstorz.com/artifactory/api/conan/enterprise-conan-dev
# https://docs.conan.io/en/latest/versioning/revisions.html
conan config set general.revisions_enabled=1
# Due to storz using a self signed certificate.
conan config set general.cacert_path='/etc/ssl/certs/ca-certificates.crt'
# login to the storz artifactory
conan user --password "$YOUR_PWD" --remote storz $YOUR_USER
```

There are [more ways on how to install](https://docs.conan.io/en/latest/installation.html#install-with-pip-recommended) on different systems.

## let's use some dependencies

let's build the project for the first time.
- Open `project_0` in your favorite text-editor. Preferable `Visual Code`.
- build the project according to the [README.md](project_0/README.md)
- this should result in the error `fatal error: fmt/format.h: No such file or directory`

The project uses the dependency [`fmt`](https://fmt.dev/latest/index.html), which provides the well known function `format("the answer is: {}", 42)`.
You might know the `format` function from other languages like `rust`, `swift`, `python`...

Now let's install the `fmt`.
- checkout [`conanfile.txt`](project_0/conanfile.txt). Under `requires` it declares which dependencies the project needs.
- let's install the needed dependencies:
```bash
conan install --remote conancenter --generator cmake_find_package --install-folder dependencies .
```
This line installs the dependencies defined in the `conanfile.txt` and installs the resulting generator output into the folder `dependencies`.
The [remote](https://docs.conan.io/en/1.36/reference/commands/misc/remote.html) is a list of server hosting receipes. Eg the storz artifactory is a `remote`. You can list all remotes by running `conan remote list`.
A [generator](https://docs.conan.io/en/latest/reference/generators.html) is a very powerful conan tool. It generates the scripts for the build system, that uses the dependencies.
- let's checkout the [dependencies](project_0/dependencies) folder.
- conan generated a file calles `Findfmt.cmake`, perfect for `find_package(fmt)`
- for the sake of learning about generatorss lets another less cluttered generator.
Run
```bash
conan install --remote conancenter --generator gcc --install-folder dependencies_gcc .
```
Then checkout the generated [conanbuildinfo.gcc](./project_0/dependencies_gcc/conanbuildinfo.gcc) 
- Let's continue with our little CMake project and let's modify [CMakeLists.txt](project_0/CMakeLists.txt) and comment in the `find_package` and `target_link_libraries` lines.
- reconfigure `cmake -S . -B build -DCMAKE_MODULE_PATH=dependencies`
- build once more `cmake --build build`
- execute the result: `./project_0/build/fun`
- know the answer! 😉

## `project_1` is in need for json! conan to the rescue!

Everyone for themself, let's find and add the dependency nlohmann json.
to search use the [website](https://conan.io/center/) or the command line
```bash
conan search --remote conancenter nlohmann_json
# btw, `conan search` can be super slow!
```

Have fun! 😸

## lets write a conanfile.py for a dependency

- Do a walkthrough through [`project_2/lib`](project_2/lib).
- Create a package using `conan create .`.
- Show package result.
- show `conan search`
- do it in slow motion - [workflow](https://docs.conan.io/en/latest/developing_packages/package_dev_flow.html)
  - `conan source --source-folder source lib/`
  - `conan install --install-folder install lib/`
  - `conan build --source-folder source --install-folder install --build-folder build lib/`
  - `conan package --source-folder source --install-folder install --build-folder build --package-folder package lib/`

let's use our newly crafted package.

checkout the [exe](project_2/exe).
- build
  - `conan install --generator cmake_find_package --install-folder dependencies .`
  - `cmake -S . -B build -DCMAKE_MODULE_PATH=dependencies`
  - `cmake --build build`
  - `./build/fun`
  - or run [`./build.sh`](project_2/exe/build.sh)
- remind yourself that we did not have to handle the dependency of the dependency (`nlohmann::json`)
  - eg when you want to use `libcurl` you don't have to worry about `libz` or `libssl`. Conan takes over!

As you can see it's quite easy to write receipes (build scripts) in conan.
These receipe can be used for the whole development process, not just libraries/frameworks. Including toolchains (think of packing gcc, build tools (eg [cmake](https://conan.io/center/cmake), code generators, executables, big modular framworks (think [boost](https://conan.io/center/boost) or [qt](https://conan.io/center/qt))

## profiles

With profiles one can change the behavior of a build.
Checkout [`project_3`](project_3). It's an conan managed executable.
Do a walkthrough through [conanfile.py](project_3/conanfile.py).

Till now we used the default profile located at `~/.conan/profiles/default`.
But we can and should define our own profiles.
A profile defines options and settings. It's very useful for cross-compilation.
Do an example for a debug and release build.
Do an example with shared library `the_answer`.
Do a cross-compilation to windows.
Arguments can be overriden with command line arguments, eg `--setting:build_type=RelWithDebInfo`

## inspect dependencies

We have now build `the_answer` a bunch of time.
Inspect the cached pre-build results by running `conan search the_answer/0.1@`.
These packages are locally cached at `~/.conan/data/`.

check local copies of `fmt`, by running `conan search fmt/8.0.0@`.
check remote copies of `fmt`, by running `conan search fmt/8.0.0@ -r conancenter`.

- meta data about a dependency: `conan info fmt/8.0.0@ -r conancenter`.
- meta data about a local dependency: `conan info the_answer/0.1@`.
- list options and settings of a package `conan inspect fmt/8.0.0@ -r conancenter`

## create html of local build of `the_answer`

Run `conan search --table fun.html the_answer/0.1@`. Inspect the `fun.html`.

## upload

These build packages may be uploaded to an artifactory using `conan upload --all --remote storz the_answer/0.1@`,
so the already build packages don't have to be build again, when needed.

## links

- [conan center with lot's of receipes](https://conan.io/center/)
- [reference](https://docs.conan.io/en/latest/reference.html)

## thanks

If there's time, show [asan](https://en.wikipedia.org/wiki/AddressSanitizer)!

